import Style from "../../App.module.css";
import React from "react";

const SelectComponent = ({selectOption, name, formData}) => {
    return (
        <>
            <div className={Style.style_form}>
                <select {...formData.getFieldProps(name)} name={name}>
                    {selectOption.map((item) => {
                        return <option key={Math.random()*100} value={item.value}>{item.label}</option>
                     })
                    })
                </select>

                {formData.errors[name]
                && formData.touched[name]
                && <div className={Style.error}>{formData.errors[name]}</div>}

            </div>
        </>
    )
};
export default SelectComponent;