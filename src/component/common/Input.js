import Style from "../../App.module.css";

const Input = ({label, name, formData,id ,type = "text"}) => {
    return (
        <div className={Style.style_form}>
            <label htmlFor={id}>{label}</label>
            <input
                id={id}
                name={name}
                type={type}
                {...formData.getFieldProps(name)}/>
            {formData.errors[name]
            && formData.touched[name]
            && <div className={Style.error}>{formData.errors[name]}</div>}
        </div>
    )
};
export default Input;