import Style from "../../App.module.css";
import React from 'react'
const CheckBox = ({formData, selectCheckBoxOption, name}) => {
    return (
        <div className={Style.style_radio}>
            {selectCheckBoxOption.map((item)=>{
                return <React.Fragment key={item.value}>
                    <label htmlFor={item.value}>{item.label}</label>
                    <input type="checkbox"
                           name={name}
                           id={item.value}
                           value={item.value}
                           onChange={formData.handleChange}
                           checked={formData.values[name].includes(item.value)}

                    />
                </React.Fragment>
            })}
            {formData.errors[name]
            && formData.touched[name]
            && <div className={Style.error}>{formData.errors[name]}</div>}
        </div>
    )
};
export default CheckBox;