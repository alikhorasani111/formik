import Style from "../../App.module.css";
import React from "react";

const Radio = ({radioValue, formData, name}) => {
    return (
        <div className={Style.style_radio}>
            {radioValue.map((item) => {
                return <React.Fragment key={item.value}>
                    <label htmlFor={item.value}>{item.label}</label>
                    <input
                        type="radio"
                        name={name}
                        value={item.value}
                        id={item.value}
                        onChange={formData.handleChange}
                        checked={formData.values[name] === item.value}/>
                    {formData.errors[name]
                    && formData.touched[name]
                    && <div className={Style.error}>{formData.errors[name]}</div>}
                </React.Fragment>
            })}
        </div>

    )
};
export default Radio;