import React, {useState, useEffect} from 'react'
import Style from '../App.module.css'
import {useFormik} from "formik";
import * as Yup from 'yup';
import axios from "axios";
import Input from "./common/Input";
import Radio from "./common/Radio";
import SelectComponent from "./common/SelectComponent";
import CheckBox from "./common/CheckBox";


const radioValue = [
    {label: "male", value: "0"},
    {label: "female", value: "1"},
];
const selectCheckBoxOption = [
    {label: "React.js", value: "React.js"},
    {label: "Vue.js", value: "Vue.js"},
];
const selectOption = [
    {label: "select nationality", value: ""},
    {label: "Iran", value: "IR"},
    {label: "Germany", value: "GER"},
    {label: "Usa", value: "US"},
];
const initialValues = {
    id: "",
    name: "",
    email: "",
    password: "",
    phone: "",
    passwordConfirmation: "",
    gender: "",
    nationality: "",
    study: [],
    terms: false,
};

const onSubmit = (values) => {
    // axios.post('http://localhost:3001/users', values)
    //     .then((res) => console.log(res))
    //     .catch((e) => {
    //         console.log(e)
    //     })
};

const SignUpForm = () => {
    const [formValues, setFormValues] = useState(null);
    const validationSchema = Yup.object({
        name: Yup.string().required('name is required').min(3, 'name length is not valid '),
        email: Yup.string().email('is not email format').required('email is not required'),
        password: Yup.string().required('password is not required').matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/,
            "must content 8 character,one uppercase ,one Lowercase ,one number and one special"),
        phone: Yup.string().required('phone is not required').matches(/^[0-9]{11}$/, 'invalid phone').nullable(),
        passwordConfirmation: Yup.string().required('password confirmation is not required').oneOf([Yup.ref('password'), null], 'password not match'),
        gender: Yup.string().required('gender is required'),
        nationality: Yup.string().required('nationality is required'),
        study: Yup.array().min(1).required('checkbox is required'),
        terms: Yup.boolean().required('the terms and condition must be accepted')
            .oneOf([true], "the terms and condition must be accepted")

    });

    const formData = useFormik(
        {
            validateOnMount: true,
            initialValues: formValues || initialValues,
            enableReinitialize: true,
            onSubmit: (values) => {
                // axios.post('http://localhost:3001/users',values)
                //     .then((res)=>console.log(res))
                //     .catch((error)=>console.log(error))
                console.log(values)
            },
            validationSchema,
        });

    useEffect(() => {
        const values = async () => {
            try {
                const {data} = await axios.get('http://localhost:3001/users/1');
                setFormValues(data)
            } catch (err) {
                console.log(err)
            }
        };
        return values()
    }, []);


    return (
        <div className={Style.container}>
            <form onSubmit={formData.handleSubmit} className={Style.box}>
                {/*<div className={Style.style_form}>*/}
                {/*    <label>First Name</label>*/}
                {/*    <input*/}
                {/*        name="name"*/}
                {/*        type="text"*/}
                {/*        {...formData.getFieldProps('name')}/>*/}

                {/*    {formData.errors.name*/}
                {/*    && formData.touched.name*/}
                {/*    && <div className={Style.error}>{formData.errors.name}</div>}*/}
                {/*</div>*/}
                <Input id="FirstName" formData={formData} name="name" label="FirstName"/>
                <Input id="email" formData={formData} name="email" label="email"/>
                <Input id="phone" formData={formData} name="phone" label="phone"/>

                <Input formData={formData}
                       id="password"
                       name="password"
                       label="password"
                       type="password"/>

                <Input formData={formData}
                       id="passwordConfirmation"
                       name="passwordConfirmation"
                       label="passwordConfirmation"
                       type="password"/>

                <SelectComponent selectOption={selectOption} formData={formData} name="nationality"/>

                <Radio radioValue={radioValue} formData={formData} name="gender"/>

                <CheckBox selectCheckBoxOption={selectCheckBoxOption} formData={formData} name="study"/>

                <label htmlFor="terms">terms and condition</label>
                <input type="checkbox"
                       name="terms"
                       id="terms"
                       value={true}
                       onChange={formData.handleChange}
                       checked={formData.values.terms}

                />
                {formData.errors.terms
                && formData.touched.terms
                && <div className={Style.error}>{formData.errors.terms}</div>}

                <div>
                    <button className={formData.isValid ? Style.button : null} type="submit"
                            disabled={!formData.isValid}>Submit
                    </button>
                </div>

            </form>
        </div>
    )
};


export default SignUpForm;


